var video = document.getElementsByClassName('first-vedio');
let playButton = document.getElementById('play-pause');
let fullScreenButton = document.getElementById('full-screen');
var progressBar = document.getElementById('progress-bar');

playButton.addEventListener('click', () => {
  if (video[0].paused && video[1].paused == true) {
    video[0].play();
    video[1].play();
    // video[1].muted = false;
    // video[0].muted = false;

    playButton.style.opacity = '0';
    playButton.innerHTML = `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M37.9692 24.3117L19.6358 11.8117C19.38 11.6384 19.05 11.6192 18.7767 11.7634C18.5042 11.9075 18.3333 12.1909 18.3333 12.5V37.5C18.3333 37.8092 18.5042 38.0925 18.7775 38.2367C18.8992 38.3017 19.0333 38.3334 19.1667 38.3334C19.3308 38.3334 19.495 38.2842 19.6358 38.1884L37.9692 25.6884C38.1967 25.5334 38.3333 25.2759 38.3333 25C38.3333 24.7242 38.1967 24.4667 37.9692 24.3117ZM20 35.9225V14.0775L36.0208 25L20 35.9225Z" fill="white"/>
        <path d="M25 0C11.215 0 0 11.215 0 25C0 38.785 11.215 50 25 50C38.785 50 50 38.785 50 25C50 11.215 38.785 0 25 0ZM25 48.3333C12.1342 48.3333 1.66667 37.8658 1.66667 25C1.66667 12.1342 12.1342 1.66667 25 1.66667C37.8658 1.66667 48.3333 12.1342 48.3333 25C48.3333 37.8658 37.8658 48.3333 25 48.3333Z" fill="white"/>
        </svg>`;
  } else {
    video[0].pause();
    video[1].pause();
    playButton.style.opacity = '1';
    playButton.innerHTML = `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M37.9692 24.3117L19.6358 11.8117C19.38 11.6384 19.05 11.6192 18.7767 11.7634C18.5042 11.9075 18.3333 12.1909 18.3333 12.5V37.5C18.3333 37.8092 18.5042 38.0925 18.7775 38.2367C18.8992 38.3017 19.0333 38.3334 19.1667 38.3334C19.3308 38.3334 19.495 38.2842 19.6358 38.1884L37.9692 25.6884C38.1967 25.5334 38.3333 25.2759 38.3333 25C38.3333 24.7242 38.1967 24.4667 37.9692 24.3117ZM20 35.9225V14.0775L36.0208 25L20 35.9225Z" fill="white"/>
        <path d="M25 0C11.215 0 0 11.215 0 25C0 38.785 11.215 50 25 50C38.785 50 50 38.785 50 25C50 11.215 38.785 0 25 0ZM25 48.3333C12.1342 48.3333 1.66667 37.8658 1.66667 25C1.66667 12.1342 12.1342 1.66667 25 1.66667C37.8658 1.66667 48.3333 12.1342 48.3333 25C48.3333 37.8658 37.8658 48.3333 25 48.3333Z" fill="white"/>
        </svg>`;
  }
});

video[0].addEventListener(
  'ended',
  () => {
    this.pause();
    $('#myModal').modal('show');
  },
  false
);

video[0].addEventListener('timeupdate', () => {
  var percentage = Math.floor((100 / video[0].duration) * video[0].currentTime);
  progressBar.value = percentage;
  $(progressBar).css('width', percentage + '%');
});

function btnActive() {
  var a = document.getElementsByClassName('btn-custom');
  for (let i = 0; i < a.length; i++) {
    a[i].addEventListener('click', function() {
      var current = document.getElementsByClassName('active');
      current[0].className = current[0].className.replace(' active', '');
      this.className += ' active';
      let dataSection = $(current[0]).data('year');
      aciveItemclass(dataSection);
    });
  }
}

function aciveItemclass(a) {
  console.log($(video).removeAttr('muted'));
  var b = document.getElementsByClassName('first-vedio');
  for (let i = 0; i < b.length; i++) {
    let element = b[i];
    if (a == element.classList[2]) {
      b[i].classList.remove('two');
      b[i].classList.add('one');
    } else {
      b[i].classList.remove('one');
      b[i].classList.add('two');
    }
  }
}
btnActive();

// iso
// var isIphone = /(iPhone)/i.test(navigator.userAgent);
// var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
// if (isIphone && isSafari) {
//   alert("Please go andriod");
// }

// fileupload
