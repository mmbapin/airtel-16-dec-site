var video = document.getElementsByClassName('first-vedio');
let playButton = document.getElementById('play-pause');
let fullScreenButton = document.getElementById('full-screen');
var progressBar = document.getElementById('progress-bar');
var logo = document.querySelectorAll('.main-navigation-section');
var ready = true;
var playButton1 = document.getElementById('plays-pause');
function StartPlay() {
  if (video[0].paused && video[1].paused == true) {
    if (ready == true) {
      video[0].currentTime = video[1].currentTime;
      video[0].play();
      video[1].play();
      playButton.style.opacity = '0';
      // $("#plays-pause svg").html(
      //   `<svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="-45 0 327 327" width="512px"><path d="m158 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"></path><path d="m8 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"></path></svg>`
      // );
      $('#plays-pause').html(
        '<svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="-45 0 327 327" width="512px"><path d="m158 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"></path><path d="m8 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"></path></svg>'
      );

      playButton.innerHTML = `<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M37.9692 24.3117L19.6358 11.8117C19.38 11.6384 19.05 11.6192 18.7767 11.7634C18.5042 11.9075 18.3333 12.1909 18.3333 12.5V37.5C18.3333 37.8092 18.5042 38.0925 18.7775 38.2367C18.8992 38.3017 19.0333 38.3334 19.1667 38.3334C19.3308 38.3334 19.495 38.2842 19.6358 38.1884L37.9692 25.6884C38.1967 25.5334 38.3333 25.2759 38.3333 25C38.3333 24.7242 38.1967 24.4667 37.9692 24.3117ZM20 35.9225V14.0775L36.0208 25L20 35.9225Z" fill="white"/>
        <path d="M25 0C11.215 0 0 11.215 0 25C0 38.785 11.215 50 25 50C38.785 50 50 38.785 50 25C50 11.215 38.785 0 25 0ZM25 48.3333C12.1342 48.3333 1.66667 37.8658 1.66667 25C1.66667 12.1342 12.1342 1.66667 25 1.66667C37.8658 1.66667 48.3333 12.1342 48.3333 25C48.3333 37.8658 37.8658 48.3333 25 48.3333Z" fill="white"/>
        </svg>`;
      window.gtag('event', 'Play', {
        event_category: 'video',
        event_label: 'Video start'
      });
    }
  } else {
    // btnActive();
    video[0].currentTime = video[1].currentTime;
    toggleButtons();
  }
}
playButton1.addEventListener('click', function() {
  $('#play-pause').css('opacity', '0');

  if (video[0].paused != true || video[1].paused != true) {
    video[0].pause();
    video[1].pause();
    playButton1.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 41.999 41.999" style="enable-background:new 0 0 41.999 41.999;" xml:space="preserve" width="512px" height="512px">
  <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40  c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20  c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z" fill="#D80027"/>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  <g>
  </g>
  </svg>`;
  } else {
    video[0].play();
    video[1].play();
    playButton1.innerHTML = `<?xml version="1.0"?>
  <svg xmlns="http://www.w3.org/2000/svg" height="512px" viewBox="-45 0 327 327" width="512px"><path d="m158 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"/><path d="m8 0h71c4.417969 0 8 3.582031 8 8v311c0 4.417969-3.582031 8-8 8h-71c-4.417969 0-8-3.582031-8-8v-311c0-4.417969 3.582031-8 8-8zm0 0" fill="#D80027"/></svg>`;
  }
});

playButton.addEventListener('click', () => {
  StartPlay();
  window.gtag('event', 'toggle', {
    event_category: 'video',
    event_label: 'Video'
  });
});

function toggleButtons() {
  var buttons = document.getElementsByClassName('btn-custom');

  var button1 = buttons[0];
  var button2 = buttons[1];

  if ($(button1).hasClass('active')) {
    $(button2).click();

    window.gtag('event', 'toggle', {
      event_category: 'video',
      event_label: '1971 Video'
    });
  } else {
    $(button1).click();

    window.gtag('event', 'toggle', {
      event_category: 'video',
      event_label: '2018 Video'
    });
  }
}

video[0].addEventListener(
  'ended',
  () => {
    // this.pause();
    $('#myModal').modal('show');
  },
  false
);

function btnActive() {
  var a = document.getElementsByClassName('btn-custom');
  for (let i = 0; i < a.length; i++) {
    a[i].addEventListener('click', function() {
      window.gtag('event', 'toggle', {
        event_category: 'video',
        event_label: ' Video'
      });
      var current = document.getElementsByClassName('active');
      current[0].className = current[0].className.replace(' active', '');
      this.className += ' active';
      let dataSection = $(current[0]).data('year');
      aciveItemclass(dataSection);
    });
  }
}

function aciveItemclass(a) {
  var b = document.getElementsByClassName('first-vedio');
  for (let i = 0; i < b.length; i++) {
    let element = b[i];
    if (a == element.classList[2]) {
      b[i].classList.remove('two');
      b[i].classList.add('one');
      $('#dash2').prop('muted', false);
      $('#dash1').prop('muted', true);
    } else {
      b[i].classList.remove('one');
      b[i].classList.add('two');
      $('#dash2').prop('muted', true);
      $('#dash1').prop('muted', false);
    }
  }
}
btnActive();

// fileupload
var data;
video[0].ontimeupdate = function() {
  // video[0].currentTime = video[1].currentTime;

  var percentage = (video[0].currentTime / video[0].duration) * 100;
  $('#custom-seekbar span').css('width', percentage + '%');
  // console.log("1", percentage);
};

window.currentVideoTime = 0;
window.currentVideoTimeHit = [];

video[1].ontimeupdate = function() {
  var percentage = (video[1].currentTime / video[1].duration) * 100;
  $('#custom-seekbar span').css('width', percentage + '%');
  // console.log("2", parseInt(percentage));
  var seconds = parseInt(video[1].currentTime);

  // console.log(window.currentVideoTimeHit);
  // console.log(seconds);

  if (seconds == 10) {
    if (!window.currentVideoTimeHit.includes('HIT_10_SEC')) {
      window.gtag('event', 'HIT_10_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '10_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_10_SEC') == -1) {
        array.push('HIT_10_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 10 && seconds < 10 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }

  if (seconds == 20) {
    if (!window.currentVideoTimeHit.includes('HIT_20_SEC')) {
      window.gtag('event', 'HIT_20_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '20_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_20_SEC') == -1) {
        array.push('HIT_20_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 20 && seconds < 20 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }

  if (seconds == 30) {
    if (!window.currentVideoTimeHit.includes('HIT_30_SEC')) {
      window.gtag('event', 'HIT_30_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '30_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_30_SEC') == -1) {
        array.push('HIT_30_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 30 && seconds < 30 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }
  if (seconds == 40) {
    if (!window.currentVideoTimeHit.includes('HIT_40_SEC')) {
      window.gtag('event', 'HIT_40_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '40_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_40_SEC') == -1) {
        array.push('HIT_40_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 40 && seconds < 30 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }
  if (seconds == 50) {
    if (!window.currentVideoTimeHit.includes('HIT_50_SEC')) {
      window.gtag('event', 'HIT_50_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '50_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_50_SEC') == -1) {
        array.push('HIT_50_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 50 && seconds < 30 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }
  if (seconds == 60) {
    if (!window.currentVideoTimeHit.includes('HIT_60_SEC')) {
      window.gtag('event', 'HIT_60_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '60_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_60_SEC') == -1) {
        array.push('HIT_60_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 60 && seconds < 30 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }
  if (seconds == 70) {
    if (!window.currentVideoTimeHit.includes('HIT_70_SEC')) {
      window.gtag('event', 'HIT_70_SEC', {
        event_category: 'WATCH_DURATION',
        event_label: '70_SEC'
      });
      var array = window.currentVideoTimeHit;
      if (array.indexOf('HIT_70_SEC') == -1) {
        array.push('HIT_70_SEC');
        window.currentVideoTimeHit = array;
      }
    }
  } else {
    if (seconds > 70 && seconds < 30 + 5 + 5) {
      window.currentVideoTimeHit = [];
    }
  }
};

$('#custom-seekbar').on('click', function(e) {
  var offset = $(this).offset();
  var left = e.pageX - offset.left;
  var totalWidth = $('#custom-seekbar').width();
  var percentage = left / totalWidth;
  var vidTime = video[0].duration * percentage;
  video[0].currentTime = vidTime;
});
$('#custom-seekbar').on('click', function(e) {
  var offset = $(this).offset();
  var left = e.pageX - offset.left;
  var totalWidth = $('#custom-seekbar').width();
  var percentage = left / totalWidth;
  var vidTime = video[1].duration * percentage;
  video[1].currentTime = vidTime;
});
